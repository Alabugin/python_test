string = "[{(}]"

bracketMapping = {')':'(','}':'{',']':'['}

def correctBrcSeq(string):
    if len(string) == 0:
        raise IOError("You enter an empty string!")
    stack = list()
    for ch in string:
        if ch in bracketMapping.values():
            stack.append(ch)
        elif ch in bracketMapping.keys():
            if not(len(stack) != 0 and bracketMapping[ch] == stack.pop()):
                return False
        else:
            raise IOError("Your bracket sequence contains no-bracket character! ")
    if len(stack) != 0:
        return False
    else:
        return True

if correctBrcSeq(string):
    print(string,"is correct")
else:
    print(string, "is incorrect")